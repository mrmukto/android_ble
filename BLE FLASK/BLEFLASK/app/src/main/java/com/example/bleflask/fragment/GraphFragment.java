package com.example.bleflask.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bleflask.graph.MyXAxisValueFormatter;
import com.example.bleflask.R;
import com.example.bleflask.graph.XYMarkerViewHumidity;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;


public class GraphFragment extends Fragment {
    BarChart barChart, barChartWeek;


    public GraphFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_graph, container, false);
        barChart = view.findViewById(R.id.barchart);
        barChartWeek = view.findViewById(R.id.barchartWeek);

        ArrayList<String> x_axis_time = new ArrayList<>();
        ArrayList<Float> y_axis_humidity = new ArrayList<>();

        ArrayList<String> x_axis_days = new ArrayList<>();
        ArrayList<Float> y_axis_humidity_week = new ArrayList<>();


        // Dummy data for humidity and time (replace this with your actual data)
        for (int i = 0; i < 10; i++) {
            x_axis_time.add(String.valueOf((i + 1)));
            y_axis_humidity.add((float) (Math.random() * 100)); // Random humidity values between 0 to 100
        }

        ArrayList<BarEntry> dataValuesHumidity = new ArrayList<>();
        for (int i = 0; i < y_axis_humidity.size(); i++) {
            dataValuesHumidity.add(new BarEntry(i, y_axis_humidity.get(i)));
        }

        // Dummy data for humidity and days (replace this with your actual data)
        String[] daysOfWeek = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        for (int i = 0; i < 7; i++) {
            x_axis_days.add(daysOfWeek[i]);
            y_axis_humidity_week.add((float) (Math.random() * 100)); // Random humidity values between 0 to 100
        }

        ArrayList<BarEntry> dataValuesHumidity_week = new ArrayList<>();
        for (int i = 0; i < y_axis_humidity_week.size(); i++) {
            dataValuesHumidity_week.add(new BarEntry(i, y_axis_humidity_week.get(i)));
        }

        BarDataSet humidityBarDataSet = new BarDataSet(dataValuesHumidity, "Humidity");
        humidityBarDataSet.setColor(getContext().getResources().getColor(R.color.refresh_green));
        humidityBarDataSet.setValueTextColor(getContext().getResources().getColor(R.color.black));
        humidityBarDataSet.setValueTextSize(12f);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(humidityBarDataSet);

        BarData barData = new BarData(dataSets);
        barChart.setExtraBottomOffset(5);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setData(barData);
        barChart.setFitBars(true);
        barChart.invalidate();


        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(x_axis_time));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setLabelCount(x_axis_time.size()); // Adjust as needed
        xAxis.setTextColor(getContext().getResources().getColor(R.color.black));
        xAxis.setTextSize(12);
        xAxis.setDrawGridLines(false);
        humidityBarDataSet.setDrawValues(true);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setAxisMinimum(0);
        leftAxis.setAxisMaximum(100); // Assuming humidity values are between 0 to 100
        leftAxis.setTextColor(getContext().getResources().getColor(R.color.black));
        leftAxis.setTextSize(12);
        xAxis.setLabelRotationAngle(-20);

        // Hide right Y-axis
        barChart.getAxisRight().setEnabled(false);

        // Hide legend
        barChart.getLegend().setEnabled(false);

        // Hide description

        ValueFormatter xAxisFormatter = new MyXAxisValueFormatter(x_axis_time.toArray(new String[x_axis_time.size()]));
        barChart.getDescription().setEnabled(false);
        XYMarkerViewHumidity mv = new XYMarkerViewHumidity(getContext(), xAxisFormatter);
        mv.setChartView(barChart); /** For bounds control **/
        barChart.setMarker(mv); /** Set the marker to the chart **/



        BarDataSet humidityBarDataSet_week = new BarDataSet(dataValuesHumidity_week, "Humidity");
        humidityBarDataSet_week.setColor(getContext().getResources().getColor(R.color.refresh_green));
        humidityBarDataSet_week.setValueTextColor(getContext().getResources().getColor(R.color.black));
        humidityBarDataSet_week.setValueTextSize(12f);
        humidityBarDataSet_week.setDrawValues(true); // Show labels on top of each bar

        ArrayList<IBarDataSet> dataSets_week = new ArrayList<>();
        dataSets_week.add(humidityBarDataSet_week);

        BarData barData_week = new BarData(dataSets_week);
        barChartWeek.setExtraBottomOffset(5);
        barChartWeek.setDoubleTapToZoomEnabled(false);
        barChartWeek.setData(barData_week);
        barChartWeek.setFitBars(true);
        barChartWeek.invalidate();

        XAxis xAxis_week = barChartWeek.getXAxis();
        xAxis_week.setValueFormatter(new IndexAxisValueFormatter(x_axis_days));
        xAxis_week.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis_week.setGranularity(1);
        xAxis_week.setLabelCount(x_axis_days.size()); // Adjust as needed
        xAxis_week.setTextColor(getContext().getResources().getColor(R.color.black));
        xAxis_week.setTextSize(12);
        xAxis_week.setDrawGridLines(false);

        YAxis leftAxis_week = barChartWeek.getAxisLeft();
        leftAxis_week.setAxisMinimum(0);
        leftAxis_week.setAxisMaximum(100); // Assuming humidity values are between 0 to 100
        leftAxis_week.setTextColor(getContext().getResources().getColor(R.color.black));
        leftAxis_week.setTextSize(12);

        // Hide right Y-axis
        barChartWeek.getAxisRight().setEnabled(false);

        // Hide legend
        barChartWeek.getLegend().setEnabled(false);

        // Hide description
        barChartWeek.getDescription().setEnabled(false);

        // Set marker
        ValueFormatter xAxisFormatter_week = new IndexAxisValueFormatter(x_axis_days);
        XYMarkerViewHumidity mv_week = new XYMarkerViewHumidity(getContext(), xAxisFormatter_week);
        mv_week.setChartView(barChartWeek);
        barChartWeek.setMarker(mv_week);


        return view;
    }
}