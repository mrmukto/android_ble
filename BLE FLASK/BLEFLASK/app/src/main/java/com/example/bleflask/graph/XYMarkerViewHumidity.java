package com.example.bleflask.graph;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.example.bleflask.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.DecimalFormat;

@SuppressLint("ViewConstructor")
public class XYMarkerViewHumidity extends MarkerView {
    private final TextView txtValue, txtTime;
    private final ValueFormatter xAxisValueFormatter;

    private final DecimalFormat format;

    public XYMarkerViewHumidity(Context context, ValueFormatter xAxisValueFormatter) {
        super(context, R.layout.custom_marker_view);

        this.xAxisValueFormatter = xAxisValueFormatter;
        txtValue = findViewById(R.id.txtValue);
        txtTime = findViewById(R.id.txtTime);
        format = new DecimalFormat("##");
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        Log.e("value: ", "" + e + highlight);
        Log.e("checkIndex: ", "" + highlight.getDataSetIndex());


        float value = e.getY();
        String time = xAxisValueFormatter.getFormattedValue(e.getX());

        if (highlight.getDataSetIndex() == 0) {
            txtValue.setText(String.format("Humidity: %s ", value));
        } else if (highlight.getDataSetIndex() == 1) {
            txtValue.setText(String.format("Customer: %s", value));
            txtTime.setText(String.format("Time: %s", time));
        }


        txtTime.setText(String.format("Time: %s", xAxisValueFormatter.getFormattedValue(e.getX())));

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
