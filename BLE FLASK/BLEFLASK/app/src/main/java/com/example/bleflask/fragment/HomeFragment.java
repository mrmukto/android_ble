package com.example.bleflask.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bleflask.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;


public class HomeFragment extends Fragment {
    PieChart pieChart;

    public HomeFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_home, container, false);
        pieChart = view.findViewById(R.id.piChart);

        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(65, ""));
        pieEntries.add(new PieEntry(35, ""));

        PieDataSet pieDataSet = new PieDataSet(pieEntries, "Humidity");
        pieDataSet.setColors(getContext().getResources().getColor(R.color.refresh_green), Color.GRAY);
        pieDataSet.setValueTextColor(Color.BLACK);
        pieDataSet.setValueTextSize(12f);

        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.invalidate();

        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setHoleRadius(70f);
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setEntryLabelTextSize(12f);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDrawCenterText(true);
        String centerText = "65%\nRemaining\n35%";

        SpannableString spannableString = new SpannableString(centerText);
        spannableString.setSpan(new RelativeSizeSpan(2f), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // Enlarge "65%"
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // Make "65%" bold

        pieChart.setCenterText(spannableString);

        pieChart.setCenterTextSize(14f);
        pieChart.setCenterTextColor(Color.BLACK);
        return  view;
    }
}